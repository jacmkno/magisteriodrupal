<?php
chdir(dirname(__FILE__).'/../../../../../');

define('DRUPAL_ROOT', getcwd());

include_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

drupal_goto('http://www.tinformatica.com/ADN/ART_MOD2'.format_date(time(), 'custom', 'd-m-Y').'.csv');
//drupal_goto('http://www.magisterio.com.co/sites/default/files/csv_files/cronstock_2015-05-21.csv');
//drupal_goto('http://www.magisterio.com.co/ART_MOD2'.format_date(time()-3600*48, 'custom', 'd-m-Y').'.csv');