(function($) {
  Drupal.behaviors.magisterio_user = {
    attach: function (context, settings) {
      var pathname = window.location.pathname;
      $('a[href="/user/logout"]').attr('href','/user/logout?destination=' + pathname);
      $('form[action="/user/register"]').attr('action','/user/register?destination=' + pathname);
      //Son paginas privadas por lo que al deslogearse se topa con un mensaje de error.
      if ($.inArray('user',window.location.pathname.split("/")) >=0 || $.inArray('orders',window.location.pathname.split("/")) >=0 || $.inArray('cart',window.location.pathname.split("/")) >=0 ) {
        $('a[href^="/user/logout"]').attr('href','/user/logout');
      }
    }
  };
})(jQuery);
