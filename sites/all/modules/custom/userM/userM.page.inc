<?php
/**
 * Functions for perfil change password
 * @param unknown $form
 * @param unknown $form_state
 * @return multitype:string number boolean NULL
 */

function change_password_m($form, &$form_state) {
  global $user;
  if (!user_is_logged_in()) {
    drupal_access_denied();
  }
  module_load_include('inc', 'user', 'user.pages');
  $form = drupal_get_form('user_profile_form', user_load($user->uid));
  $form['#attributes']['class'][] = 'user_profile_form-change-password';
  //$request_new = l(t('Request new password'), 'user/password', array('attributes' => array('title' => t('Request new password via e-mail.'))));
  //$current_pass_description = t('Enter your current password to change the %pass. !request_new.', array('%pass' => t('Old Password'), '!request_new' => $request_new));
  //$form['account']['current_pass']['#description'] = $current_pass_description;
  if (TRUE) { /* FLAG TO REMOVE */
    unset(
      $form['account']['name'],
      $form['account']['mail'],
      $form['account']['mail'],
      $form['account']['status'],
      $form['account']['roles'],
      $form['field_nombre_s_'],
      $form['field_lugar_de_trabajo'],
      $form['field_user_ubicaci_n'],
      $form['field_caracter_institucion'],
      $form['field_zona_institucion'],
      $form['field_has_participado'],
      $form['field_terminos'],
      $form['field_fecha_de_nacimiento'],
      $form['picture'],
      $form['timezone'],
      $form['field_n_mero_identificaci_n'],
      $form['field_tipo_identificaci_n'],
      $form['field_apellidos'],
      $form['field_tel_fono_contacto'],
      $form['field_cargo'],
      $form['field_ocupaci_n'],
      $form['field_preferencias'],
      $form['field_c_digo_postal'],
      $form['field_nivel_de_ensenanza'],
      $form['field_newsletter'],
      $form['field_vecino_parkway'],
      $form['field_barrio'],
      $form['field_direccion']);
      /*
      $form['account']['name']['#access'] 			= 0;
      $form['account']['mail']['#access'] 			= 0;
      $form['account']['status']['#access'] 			= 0;
      $form['account']['roles']['#access'] 			= 0;
      $form['field_nombre_s_']['#access'] 			= 0;
      $form['field_lugar_de_trabajo']['#access'] 		= 0;
      $form['field_user_ubicaci_n']['#access'] 		= 0;
      $form['field_caracter_institucion']['#access'] 	= 0;
      $form['field_zona_institucion']['#access'] 		= 0;
      $form['field_has_participado']['#access'] 		= 0;
      $form['field_terminos']['#access'] 				= 0;
      $form['field_fecha_de_nacimiento']['#access'] 	= 0;
      $form['picture']['#access'] 					= 0;
      $form['timezone']['#access'] 					= 0;
      $form['field_n_mero_identificaci_n']['#access'] = 0;
      $form['field_tipo_identificaci_n']['#access'] 	= 0;
      $form['field_apellidos']['#access'] 			= 0;
      $form['field_tel_fono_contacto']['#access'] 	= 0;
      $form['field_cargo']['#access'] 				= 0;
      $form['field_ocupaci_n']['#access'] 			= 0;
      $form['field_preferencias']['#access'] = 0;
       */
  }
  return $form;
}

/**
 * End Functions for profile changed password
 */
  

/**
 * Start function for my preferences
*/
 
function user_preferences($form, &$form_state){
  global $user;
  if (!user_is_logged_in()) {
    drupal_access_denied();
  }
  module_load_include('inc', 'user', 'user.pages');
  //$form = drupal_get_form('user_profile_form', $user);
  $form = drupal_get_form('user_profile_form', user_load($user->uid));
  $form['#attributes']['class'][] = 'user_profile_form-preferences';
  unset(
    $form['account']['current_pass'],
    $form['account']['pass'],
    $form['account']['name'],
    $form['account']['mail'],
    $form['account']['status'],
    $form['account']['roles'],
    $form['field_nombre_s_'],
    $form['field_n_mero_identificaci_n'],
    $form['field_user_ubicaci_n'],
    $form['field_terminos'],
    $form['picture'],
    $form['timezone'],
    $form['field_tipo_identificaci_n'],
    $form['field_apellidos'],
    $form['field_tel_fono_contacto'],
    $form['field_c_digo_postal'],
    $form['field_barrio'],
    $form['field_direccion']);
  //$form['field_fecha_de_nacimiento']['#access'] 	= TRUE;
  /*
  $form['account']['current_pass']['#access'] 	= FALSE;
  $form['account']['name']['#access'] 			= FALSE;
  $form['account']['mail']['#access'] 			= FALSE;
  $form['account']['status']['#access'] 			= FALSE;
  $form['account']['roles']['#access'] 			= FALSE;
  $form['field_nombre_s_']['#access'] 			= FALSE;
  $form['field_n_mero_identificaci_n']['#access'] = FALSE;
  $form['field_user_ubicaci_n']['#access'] 		= FALSE;
  $form['field_terminos']['#access'] 				= FALSE;
  $form['picture']['#access'] 					= FALSE;
  $form['timezone']['#access'] 					= FALSE;
  $form['account']['pass']['#access'] 			= FALSE;
  $form['field_n_mero_identificaci_n']['#access'] = FALSE;
  $form['field_tipo_identificaci_n']['#access'] 	= FALSE;
  $form['field_apellidos']['#access'] 			= FALSE;
  $form['field_tel_fono_contacto']['#access'] 	= FALSE;
  $form['field_fecha_de_nacimiento']['#access'] 	= TRUE;
  $form['field_lugar_de_trabajo']['#access']		= TRUE;
  $form['field_tipo_institucion']['#access']		= TRUE;
  $form['field_caracter_institucion']['#access']	= TRUE;
  $form['field_zona_institucion']['#access']		= TRUE;
  $form['field_cargo']['#access']					= TRUE;
  $form['field_ocupaci_n']['#access']				= TRUE;
  $form['field_has_participado']['#access']		= TRUE;    
*/

  return $form;  
}


function user_preferences_submit($form, &$form_state) { 
	
}
/**
 * Finally user preferences
 */


function alter_shipping($price){		
	
	$order = (array)uc_order_load($_SESSION['cart_order']);
	$product = $order['products']; 
	$tmpProduct = array();
	$i = 0;
	foreach( $product as $key => $value) {
		$tmpProduct[] = $value;
		$i++;
	}
	
	
	$query = db_select('uc_order_line_items', 'uc')
			-> fields('uc', array('line_item_id'))
			-> condition('uc.order_id', $tmpProduct[0]->order_id);
	$result = $query->execute();		
	while ($row = $result->fetchAssoc()) {
		$arraydatos = $row["line_item_id"];
	}
	
	$fields = array(
			'title' => 'Costo de envío',
			'amount' => $price,
	);
	db_update('uc_order_line_items')
	->fields($fields)
	->condition('line_item_id', $arraydatos)
	->execute();
}
