(function($){ 
  $(function(){
    (function() {
      var $formulario_correo = $('body.node-type-curso form.webform-client-form');
      if ($formulario_correo.length > 0) {
        if (!Drupal.settings.acCategoryFeatures.oculta_evento_formulario_correo) {
          $formulario_correo.hide();
        }   
      }   
    })();
    var s = $('<select name="form-item-user-city" />');
    var selects = function(){
      return $('.form-item-field-user-ubicaci-n-und-0-province', '#user-register-form, #user-profile-form').find('SELECT');
      //return $('.form-item-field-user-ubicaci-n-und-0-country, .form-item-field-user-ubicaci-n-und-0-province', '#user-register-form').find('SELECT');
      //return $('.form-item-panes-delivery-delivery-zone, .form-item-panes-billing-billing-zone', '.page-cart-checkout').find('SELECT');
    };
        
    var check_city_opts = function(data){
      selects().each(function(){
        if(!$(this).hasClass('t-rdy')){
          $(this).change(check_city_opts).addClass('t-rdy');
        }
        var cityinp = $(this).parents('FIELDSET,.fieldset-wrapper').eq(0).find('INPUT[name*="city"]');
        console.log( 'cityinp: ', cityinp.length, cityinp.data('zone'), zone );
        var zone = $(this).val();
        if(cityinp.data('zone')== zone){
          return;
        }
        
        var opts = cityinp.nextAll('.t-cities').find('ul');
        cityinp.parents('.form-item').addClass('t-cityselwrap');
        if(!opts.length){
          opts = $('<div class="t-cities"><div></div><ul></ul></div>').insertAfter(cityinp).find('ul');
          opts.parent().click(function(){
            $(this).toggleClass('t-ddopen');
          });
        }
        if(!$(this).val().length || $(this).val() == '0'){
          opts.parent().remove();
          return;
        }
        if(cityinp.data('zone')!= zone ){          
          opts.parent().addClass('t-loading');
          $.getJSON( "./?uc_tcc_cities_by_zone_user="+$(this).val(), function( data ) {
            opts.parent().removeClass('t-loading');
            cityinp.data('zone', zone);
            opts.html('');
            if(!data.length){
              opts.parent().remove();
              return;
            }
            var valisvalid = false;
            $.each( data, function( key, val ) {
              var valid = true;
              if(val.substr(val.length-1)=='*'){  
                val = val.substr(0, val.length-1);
                valid = false; // OJO desactivado
              }
              if(cityinp.val() == val){
                valisvalid = true;
              }
              var li = $("<li" + (!valid?" class='t-invalidopt'":'') + " val='" + val + "'>" + val + "</li>");
              opts.append( li );
              li.click(function(){
                cityinp.val($(this).attr('val'));
                jQuery('#quotes-pane INPUT[type=submit]').mousedown();
                if(!valid){
                  //alert('Esta ciudad no cuenta con servicio de envío, selecciona otra ciudad más cercana o contáctanos escribiéndonos a info@magisterio.com.co' );
                  alert('Lo sentimos la ciudad que seleccionaste no la cubre nuestro servicio de envíos. Por favor selecciona la ciudad que se encuentre más cerca a tu lugar de domicilio.' );
                  cityinp.val('');
                  $('#quotes-pane INPUT[type=submit]').mousedown();
                }
              });
            });
            if(!valisvalid){
              cityinp.val('');
            }
          });
        }
      });	
    };
    
    $('body.page-user').ajaxComplete(check_city_opts);
    check_city_opts();
  });
})(jQuery);

