(function($){ 
  var gotoHash_INPUT = null;
  function gotoHash(hash){
    if( !gotoHash_INPUT ){
      gotoHash_INPUT = $('.region-header .gsc-input input[name="search"]')[0];
    }
    
    if( gotoHash_INPUT && hash != $(gotoHash_INPUT).data('last') && hash.indexOf('#/buscador/') === 0 ){
      var search = decodeURIComponent(hash.split('#/buscador/').pop());
      $('.region-header .gsc-input input[name="search"]').val( search );
      $('.region-header input.gsc-search-button').click();
      console.log(hash);
      $(gotoHash_INPUT).data('last', hash);
    }
  }
  
  $(function(){
    var schg = function(){
      if($(this).val()=='-1'){
        $(this).parents('fieldset').eq(0).removeClass('t-noaddress');
      }else{
        $(this).parents('fieldset').eq(0).addClass('t-noaddress');
      }
    };
    $('.page-cart-checkout SELECT[name*="select_address"]').change(schg);
    $('.page-cart-checkout SELECT[name*="select_address"]').each(schg);

    window.setInterval(function(){
      if( window.location.hash && window.location.hash.length ){
        gotoHash(window.location.hash);
      }
    }, 300);
    
    $('.region-subheader a[href^="/buscador/"]').each(function(){
      $(this).attr('href', '#'+$(this).attr('href'));
    });

    /* El campo de company se usa como tipo de documento  */
    var set_company_as_doctype = function(){
      $('.page-cart-checkout INPUT[name*=company]').each(function(){
        var opts = $('<select></select>').attr('name', $(this).attr('name'));
        $(Drupal.settings.acCategoryFeatures.tipo_documento/*[
           ['','- Seleccionar -'],
           ['cc','Cédula'],
           ['pasaporte','Pasaporte'],
           ['ce','Cédula de extrangería'],
//           ['nit','NIT'],
//           ['dni','DNI']
           ['re','Registro Civíl']
        ]*/).each(function(i, v){
           $('<option></option>').attr('value', v[0]).text(v[1]).appendTo(opts);
        });
        opts.insertBefore(this).val($(this).val());
        $(this).remove();
      });
    };
    $('body').ajaxComplete(set_company_as_doctype);
    set_company_as_doctype();

    var selects = function(){
      return $('.form-item-panes-delivery-delivery-zone, .form-item-panes-billing-billing-zone', '.page-cart-checkout').find('SELECT');
    };
    var check_city_opts = function(data){
      selects().each(function(){
        if(!$(this).hasClass('t-rdy')){
          $(this).change(check_city_opts).addClass('t-rdy');
        }
        var cityinp = $(this).parents('FIELDSET').find('INPUT[name*="city"]');
        var zone = $(this).val();
        if(cityinp.data('zone')== zone){
          return;
        }
        
        var opts = cityinp.nextAll('.t-cities').find('ul');
        
        cityinp.parents('.form-item').addClass('t-cityselwrap');
        if(!opts.length){
          opts = $('<div class="t-cities"><div></div><ul></ul></div>').insertAfter(cityinp).find('ul');
          opts.parent().click(function(){
            $(this).toggleClass('t-ddopen');
          });
        }
        if(!$(this).val().length || $(this).val() == '0'){
          opts.parent().remove();
          return;
        }
        if(cityinp.data('zone')!= zone ){
          opts.parent().addClass('t-loading');
          $.getJSON( "./?uc_tcc_cities_by_zone="+$(this).val(), function( data ) {
            opts.parent().removeClass('t-loading');
            cityinp.data('zone', zone);
            opts.html('');
            if(!data.length){
              opts.parent().remove();
              return;
            }
            var valisvalid = false;
            $.each( data, function( key, val ) {
              var valid = true;
              if(val.substr(val.length-1)=='*'){ 
                val = val.substr(0, val.length-1);
                valid = false;
              }
              if(cityinp.val() == val){
                valisvalid = true;
              }
              var li = $("<li" + (!valid?" class='t-invalidopt'":'') + " val='" + val + "'>" + val + "</li>");
              opts.append( li );
              li.click(function(){
                cityinp.val($(this).attr('val'));
                jQuery('#quotes-pane INPUT[type=submit]').mousedown();
                if(!valid){
                  alert('Esta ciudad no cuenta con servicio de envío, selecciona otra ciudad más cercana o contáctanos escribiéndonos a info@magisterio.com.co' );
                  cityinp.val('');
                  $('#quotes-pane INPUT[type=submit]').mousedown();
                }
              });
            });
            if(!valisvalid){
              cityinp.val('');
            }
          });
          jQuery('#quotes-pane INPUT[type=submit]').mousedown(); //Forzar calculo inicial pedido
        }
      });	
    }
    $('body.page-cart-checkout').ajaxComplete(check_city_opts);
    check_city_opts();

    (function() { //Quitar el enlace al revisterio virtual, cuando no haya informacion del mismo.
      $('a.link-revistero[href=""]').parents('.panel-pane.pane-custom').remove();
    })();
    (function() { //Poner numero de orden al principio del review de la misma.
      var $t = $('table.order-review-table .pane-title-row td');
      $('table.order-review-table .pane-title-row').hide();
      $('table.order-review-table .pane-title-row:first-child').css('display','block');
      var val = $('.--ref-order-id').val();
      if (val)
        setInterval(function() { //Este vergonzoso comando se uso por que un script parecia cambiar el titulo despues de que se cambiaba una vez.
          $t.text('Resumen de la compra: Pedido ' + val);
          },1000
      );
    })();
    (function() { //Cambiar posicion informacion pago BAncolombia
      var val = $('.--ref-order-id').val();
      if (!val) return 0;
      var extra_inf = $('.t-frminfo').detach();
      extra_inf.find('.-order-id-placeholder').text(val);
      $('#review-instructions').after(extra_inf);
    })();
  });
  Drupal.behaviors.acCategoryFeatures = {
    attach: function (context, settings) {
      $('body.page-admin-store-orders- .form-item-notify').hide(); /* desactivada opcion enviar un correo al cambiar el estado del pedido, pues ahora siempre se envia */
      (function() { //Poner subsecciones en los medios de pago en el formulario de checkout
       if ($('.fieldset-legend-payment-group').length!=2) {
         $('.form-type-radio.form-item-panes-payment-payment-method:nth-child(1) label').text('')
         .append('<span class="nombre-metodo-pago">Consignación bancaria en Bancolombia</span> <a class="myFancy" href="/sites/all/imagenesfancybox/popup/pop_ups-bancol.jpg">¿comó funciona?</a> ');
         $('.form-type-radio.form-item-panes-payment-payment-method:nth-child(3)').before('<span class="fieldset-legend-payment-group">Fuera de Colombia</span>');
         $('.form-type-radio.form-item-panes-payment-payment-method:nth-child(1)').before('<span class="fieldset-legend-payment-group">En Colombia</span>');
       }
      })();
      (function() { //Poner codigos postales en 0, cuando se seleccione Colombia, y en VACIO cuando se seleccione otro pais.
        $('select.location_auto_country').change(function(e) { //editando perfil de usuario
          var val = '';
          if ($('select.location_auto_country').val() == 'co') {
            val = '0';
          }
          $('input#edit-field-c-digo-postal-und-0-value').val(val);
        });
        $('select#edit-panes-delivery-delivery-country').change(function(e) { // informacion de envio en checkout
          var val = '';
          console.log(4);
          if ($('select#edit-panes-delivery-delivery-country').val()==170) {
            val = '0';
          }
          console.log(val);
          $('input#edit-panes-delivery-delivery-postal-code').val(val);
        });
      })();
      function formatnum (val) {
        var res = val.toString();
        var l = res.length;
        if (l>3) {
          res = res.substr(0,l-3) + ',' + res.substr(l-3,3);
        }
        res = '$'+res;
        return res;
      }
      (function() { //Poner precios unitarios
        var rows = $('#uc-cart-view-form-table table tbody tr');
        var row = $(rows[0]);;
        rows.each(function (i,v) {
          var v = $(v);
          if (v.children().length==5) {
            var price = v.find('td.price').text().replace(/[^0-9]/g,"");
            var qty = v.find('td.qty input').val();
            if (qty == parseInt(qty)) {
              $(v).find('td.qty').before('<td class="uprice">'+formatnum(price/qty)+'</td>');
            }
          } else if (v.children().length==1) {
            var tot = v.find('td');
            tot.attr('colspan',1*tot.attr('colspan')+1);
          }
        });
        var head = row.parents('table').find('thead tr');
        if (head.children().length==5) {
          head.find('th:nth-child(3)').after('<th>Precio Unit.</th>');
        }
      })();
      (function() { //Poner precios unitarios cart/checkout
        var rows = $('table.cart-review tbody tr');
        var row = $(rows[0]);
        rows.each(function (i,v) {
          var v = $(v);
          if (v.children().length==3) {
            var price = v.find('td.price .uc-price').text().replace(/[^0-9]/g,"");
            var qty = v.find('td.qty').text().replace(/[^0-9]/g,"");
            if (qty == parseInt(qty)) {
              $(v).find('td.price').before('<td class="uprice">'+formatnum(price/qty)+'</td>');
            }
          } else if (v.children().length==1) {
            var tot = v.find('td');
            tot.attr('colspan',1*tot.attr('colspan')+1);
          }
        });
        var head = row.parents('table').find('thead tr');
        if (head.children().length==3) {
          head.find('th:nth-child(3)').before('<th>Precio Unit.</th>');
        }
      })();
      if ($('select[name="panes[delivery][delivery_country]"] option[value="-1"]').length==0) //revisar
        $('select[name="panes[delivery][delivery_country]"]').prepend('<option value="-1">- Seleccionar -</option>');
      $('input.t-return-home').click(function(e){ 
        e.preventDefault();
        window.location.href = "/";
      });
      if ($('form.t-checkout-review-check').length>0) { //Ocultar fila con subtotal repetido cuando se escoge consignación
        $('.page-cart-checkout-review .order-review-table tbody tr:nth-of-type(18)').hide();
      }
      if ($('body.page-user-orders-').length>0) { //Activar el item de menu Mis pedidos, si se esta en el detalle de un pedido
        $('.page-user .t-mwrapper .action-links li:nth-of-type(4), .page-user .t-mwrapper .action-links li:nth-of-type(4) a').addClass('active');
      }
      if(Drupal.settings.acCategoryFeatures) if (Drupal.settings.acCategoryFeatures.stock_disponible==false || Drupal.settings.acCategoryFeatures.stock_disponible==0) { //Desaparecer boton de compra si no hay existencias.
        var noUnit = false;
        $('body.uc-product-node.page-node #block-system-main .add-to-cart form .form-type-radios .form-radios .form-item:first-child').each(function(){
          if($(this).text().toLowerCase().split('unidad').length > 1){
            $(this).html('Producto no disponible por unidad.');
            noUnit = true;
          }
        });
        if(!noUnit){
          $('body.uc-product-node.page-node #block-system-main .add-to-cart form input.node-add-to-cart').replaceWith("<div>Producto no disponible.</div>");
        }
      }
      (function() { //Enlace todos los temas en Mis Preferencias.
        var $checkboxes = $('#user-profile-form .field-name-field-preferencias .form-checkboxes');
        var $btn = $('<a class="t-preferences-all-link" href="" >Seleccionar todos los temas</a>');
        var select = true;
        $btn.bind('click',function(e) {
            e.preventDefault();
            $btn.text(!select?"Seleccionar todos los temas":"Borrar selección de temas");
            $('input.form-checkbox',$checkboxes).attr('checked',select);
            select = !select;
          });
        $checkboxes.before($btn);
      })();
    }
  };
})(jQuery);
