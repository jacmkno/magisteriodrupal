(function($){ 
  Drupal.behaviors.textualcleanbehavior = {
    attach: function(context,settings) {
      $('.page-cart-checkout legend').replaceWith(function(){
        return $("<div />", {html: $(this).html(),class:'t-dev-legend'});
      });
      $('fieldset.location label').each(function(i,v) {
        var v = $(v);
        if (v.text().trim()=="Estado/Provincia") {
          v.text("Depto/Estado");
        }
      });
      $('.cart-block-summary-items').text($('.cart-block-summary-items').text().replace("Tienes ",""));
    }
  };
	closureInlineScript = function(){
    
		$('<span></span>').appendTo('.block-menu > h2');
		$('#block-uc-cart-cart > *').wrapAll($('.cart-block-view-cart a').clone().html(''));
		$('.block-views.contextual-links-region, .block-nodeblock.contextual-links-region').each(function(){
			if(!$('>h2', this).length){
				$(this).addClass('t-contextualborder');
			}
		});
		$('.view-display-id-productsuggestions').parents('.block-views').addClass('t-color-1');
		$('.view-display-id-commingevents').parents('.block-views').addClass('t-color-2');
		$('.block-webform label').overlapped();
                $('.block #modalContent .close').click(function(){
                    $(this).parents('#modalContent').hide();
                });
                $('.region-header a[href="/user/login"]').click(function(e){
                   $('.block #modalContent').show();
                   e.stopImmediatePropagation();
                   return false; 
                });
                

    (function(){
      /* Sacar los elementos del fieldset de location fuera del fieldset para poder manipularlos por CSS */
      $('#user-profile-form .field-widget-location fieldset > div').each(function(){
        $(this).insertBefore($(this).parent());
      });
    })();

    (function(){
      /* Aceptar los terminos debe ser un radio button */
      var fixit = function(){
        window.setTimeout(function(){
          $('.page-user .field-name-field-terminos .form-item').each(function(){
            $(this).html($(this).html().replace('type="radio"','type="checkbox"'));
          });
        }, 100);
      }
      $('body').ajaxComplete(fixit);
      fixit();
    })();

    $('.t-content FORM INPUT[type="radio"][value="pagosonlinef"]').attr('value','pagosonline');
	}
	$(function(){
		$('.t-carrousel').each(function(){
			var prev = $('<span class="t-prev"></span>').appendTo(this);
			var next = $('<span class="t-next"></span>').appendTo(this);
			carousel($('.view-content',this), prev, next, 2);
		});
/* FLAGTOREMOVE 
var unajaxify = $('#login-form, #register-form', '.page-user #block-system-main').find('form');
unajaxify.unbind('submit').find('INPUT[type=submit]').unbind('click');
*/
	}); 
  

	$(function(){    
		$("#edit-submitted-email").focus(function(){
			$('label[for="'+$(this).attr('id')+'"]').hide();
		});
    (function() { //Comportamiento menu editorial/libreria/revista/formacion
      var $activei = $('#block-system-main-menu .content > ul.menu > li.active-trail');
      var any_active = $activei.length>0;
      var $mblock = $('#block-system-main-menu');
      var mblock_h0 = 27, mblock_h1 = 60;
      var $menutopitems = $('#block-system-main-menu .content > ul.menu > li > a');
      var $menutopitems_li = $('#block-system-main-menu .content > ul.menu > li');
      /*$mblock.height(any_active?mblock_h1:mblock_h0);
      $menutopitems.mouseover(function() {
        if ($(this).parent().hasClass('active-trail')) return;
        $(this).parent().find(' > ul.menu').hide().slideDown( "slow", function() {
          // Animation complete.
        });
        if (!any_active) $mblock.animate({height:mblock_h1+'px'},500);
      });
      $menutopitems_li.mouseout(function(e) {
        var dest = e.toElement || e.relatedTarget;
        console.log('Y');
        console.log(dest);
        console.log($(this).has($(dest))?1:0);
        if ($(this).has($(dest)).length>0) return;
        if ($(this).hasClass('active-trail')) return;
        console.log('YY');
        $(this).find(' > ul.menu').show().slideUp( "slow", function() {
          // Animation complete.
        });
        if (!any_active) $mblock.animate({height:mblock_h0+'px'},500);
      }); */
      $menutopitems.click(function(){
        $menutopitems_li.removeClass('active-trail');
        $menutopitems.removeClass('active-trail');
        $(this).parent().addClass('active-trail');
        $(this).addClass('active-trail');
      });
    })();
    /* FLAG TO REMOVE
		$(".li_editorial + .menu").mouseover(function() {
			$( this ).prev().addClass("back-azul");
		}).mouseout(function() {
			$( this ).prev().removeClass("back-azul");
		})
		$(".li_libreria + .menu").mouseover(function() {
			$( this ).prev().addClass("back-rojo");
		}).mouseout(function() {
			$( this ).prev().removeClass("back-rojo");
		})
		$(".li_revista + .menu").mouseover(function() {
			$( this ).prev().addClass("back-verde");
		}).mouseout(function() {
			$( this ).prev().removeClass("back-verde");
		})
		$(".li_formacion + .menu").mouseover(function() {
			$( this ).prev().addClass("back-naranja");
		}).mouseout(function() {
			$( this ).prev().removeClass("back-naranja");
		})

		if($('#block-system-main-menu').find('.active').length !== 9){  
			menu_active = $('#block-system-main-menu').find('.active').parent();
			menu_active.find(".menu").css('display','block');
			$('#block-system-main-menu').css("height","60");
		}
     FLAG TO REMOVE */
		$( ".li_editorial" ).parent().find( "ul" ).css( "background-color", "#0070ba" );        
		var libreria= $( ".li_libreria" ).parent().find( "ul" );
		var revista= $( ".li_revista" ).parent().find( "ul" ); 
		var formacion= $( ".li_formacion" ).parent().find( "ul" ); 
		libreria.css( "background-color", "#ed1941");
		libreria.css("left", "0px");
		revista.css("background-color", "#6f9a34")
		revista.css("left", "0px");
		formacion.css("background-color", "#f68b1f")
		formacion.css("left", "0px");

	
		$('.google_books .google_books_field_name').html('');
		$('.google_books .google_books_field_data a ').html('leer algunas páginas');
		
        /* FLAGTOREMOVE
		$('.page-user #edit-mailchimp-lists-mailchimp-quiero-recibir-el-bolet-n-subscribe').attr('tabindex','12');
         FLAGTOREMOVE*/
		
		/*Recarga la pagina despues de hacer la peticion AJAX de agregar un elemento al CART*/

		$(document).ajaxComplete(function(event, jqXHR, settings){
			var mydata =  jQuery.parseJSON(jqXHR.responseText);
      var cmds = {};
      for(var i in mydata){
         // ACTIVISUAL: antes solo se revisaba el primer comando, con esto se revisan todos los comandos
         cmds[mydata[i].command] = true;
      }
			if (mydata) { 
				var numdiv = $('.formacion-general.view-cursos').find('div.views-row').length;
				var numdivhome = $('.formacion-general .view-formacion').find('div.views-row').length;
				for (var i = 1; i <= numdiv; i++){
					var fecha = jQuery('.formacion-general.view-cursos .views-row-'+i+' .views-field-field-field-fecha-inicio .field-content').text();
					var precio = jQuery('.formacion-general.view-cursos .views-row-'+i+' .views-field-sell-price span span').text();
					var link = jQuery('.formacion-general.view-cursos .views-row-'+i+' .views-field-title span a').attr('href');  
					jQuery('.formacion-general.view-cursos .views-row-'+i+'').append('<a class="btn-ver-mas-cursos" href="'+link+'">Ver más</a>');
					if (precio == '$0')
						jQuery('.formacion-general.view-cursos .views-row-'+i+' .views-field-sell-price span span').text('Entrada libre');
					if (fecha == '')
						jQuery('.formacion-general.view-cursos .views-row-'+i+' .views-field-field-field-fecha-inicio .field-content').text('Abierto');
				}
				//if( mydata[1].command === 'ucAjaxCartAltAddItemSuccess' || mydata[1].command === 'updateBuildId' )
				if( cmds.ucAjaxCartAltAddItemSuccess ){
					if ( $('body').hasClass('logged-in') ){
						location.reload();
						jQuery('body').scrollTop(0);
					}
					if (jQuery('.messages').hasClass('error')){
						location.reload();
					}
					else
						window.location.href = "/cart";
				}
				if(cmds.insert){
					$('.page-user-register select[id^="edit-field-user-ubicaci-n-und-0-province"] option:first-child').text('Selecciona');
                                        /* FLAGTOREMOVE
					jQuery('.page-user-register #edit-field-nombre-s-und-0-value').attr('tabindex','1');
					jQuery('.page-user-register #edit-field-nombre-s-und-0-value--2').attr('tabindex','1');
					jQuery('.page-user-register #edit-field-nombre-s-und-0-value--3').attr('tabindex','1');

					jQuery('.page-user-register #edit-field-user-ubicaci-n-und-0-country').attr('tabindex','2');
					jQuery('.page-user-register #edit-field-user-ubicaci-n-und-0-country--2').attr('tabindex','2');
					jQuery('.page-user-register #edit-field-user-ubicaci-n-und-0-country--3').attr('tabindex','2');

					jQuery('.page-user-register #edit-field-user-ubicaci-n-und-0-province').attr('tabindex','3');
					jQuery('.page-user-register #edit-field-user-ubicaci-n-und-0-province--2').attr('tabindex','3');
					jQuery('.page-user-register #edit-field-user-ubicaci-n-und-0-province--3').attr('tabindex','3');

					
					jQuery('.page-user-register #edit-field-user-ubicaci-n-und-0-city').attr('tabindex','4');
					jQuery('.page-user-register #edit-field-user-ubicaci-n-und-0-city--2').attr('tabindex','4');
					jQuery('.page-user-register #edit-field-user-ubicaci-n-und-0-city--3').attr('tabindex','4');

					jQuery('.page-user-register #edit-mail').attr('tabindex','5');
					jQuery('.page-user-register #edit-mail--2').attr('tabindex','5');
					jQuery('.page-user-register #edit-mail--3').attr('tabindex','5');

					
					jQuery('.page-user-register #edit-pass-pass1').attr('tabindex','6');
					jQuery('.page-user-register #edit-pass-pass1--2').attr('tabindex','6');
					jQuery('.page-user-register #edit-pass-pass1--3').attr('tabindex','6');

					jQuery('.page-user-register #edit-pass-pass2').attr('tabindex','7');
					jQuery('.page-user-register #edit-pass-pass2--2').attr('tabindex','7');
					jQuery('.page-user-register #edit-pass-pass2--3').attr('tabindex','7');

					jQuery('#edit-mailchimp-lists-mailchimp-quiero-recibir-el-bolet-n-subscribe').attr('tabindex','8');
					jQuery('#edit-mailchimp-lists-mailchimp-quiero-recibir-el-bolet-n-subscribe--2').attr('tabindex','8');
					jQuery('#edit-mailchimp-lists-mailchimp-quiero-recibir-el-bolet-n-subscribe--3').attr('tabindex','8');

					jQuery('#edit-field-terminos-und-0').attr('tabindex','9');
					jQuery('#edit-field-terminos-und-0--2').attr('tabindex','9');
					jQuery('#edit-field-terminos-und-0--3').attr('tabindex','9');
                                        */
				}

				if(cmds.viewsScrollTop){
					$.fn.revistaActual();
				}

				if(cmds.settings || cmds.insert){ 
          /* FLAGTOREMOVE MARCA:AAAA
					jQuery('.page-cart-checkout .form-radios > div:nth-of-type(2)').append('<div id="pago-colombia-id"></div>');
					jQuery('#pago-colombia-id').html('');
					jQuery('#pago-colombia-id').html('<ul class="pago-colombia"><li>Tarjeta de crédito<img src="/sites/all/imagenesfancybox/visa.gif"/><img src="/sites/all/imagenesfancybox/master.gif"/><img src="/sites/all/imagenesfancybox/american.png"/><img src="/sites/all/imagenesfancybox/diners.gif"/></li><li>Pago con tarjeta débito de ahorros o corriente<img src="/sites/all/imagenesfancybox/pse.png"/></li><li>Pago en efectivo en bancos Bancolombia, Occidente Banco de Bogotá<img src="/sites/all/imagenesfancybox/codigo.png"/></li><li>Pago en efectivo en Puntos Vía Baloto<img src="/sites/all/imagenesfancybox/baloto.gif"/></li><li>Pago en efectivo en Efecty<img src="/sites/all/imagenesfancybox/efecty.png"/></li></ul>');
				  	jQuery('.page-cart-checkout .form-radios div:nth-of-type(2) label').html('Pago en Colombia');	

				  	jQuery('.page-cart-checkout .form-radios > div:nth-of-type(1) label').html('Fuera de Colombia');
				  	jQuery('.page-cart-checkout .form-radios >  div:nth-of-type(1)').append('<div id="fuera-colombia-id"></div>');
				  	jQuery('#fuera-colombia-id').html('');
				  	jQuery('#fuera-colombia-id').html('<ul class="fuera-colombia pago-colombia"><li>Tarjeta de crédito <img src="/sites/all/imagenesfancybox/visa.gif"/><img src="/sites/all/imagenesfancybox/master.gif"/><img src="/sites/all/imagenesfancybox/american.png"/><img src="/sites/all/imagenesfancybox/diners.gif"/></li></ul>');
				  	$('.page-cart-checkout #edit-panes-payment-payment-method .form-item').append('<a class="medios-de-pago" target="_blank" href="/node/29">¿Cómo funciona?</a>');
				  	$('.page-cart-checkout #edit-panes-payment-payment-method--2 .form-item').append('<a class="medios-de-pago" target="_blank" href="/node/29">¿Cómo funciona?</a>');
				  	$('.page-cart-checkout #edit-panes-payment-payment-method--3 .form-item').append('<a class="medios-de-pago" target="_blank" href="/node/29">¿Cómo funciona?</a>');
				  	$('.page-cart-checkout #edit-panes-payment-payment-method--4 .form-item').append('<a class="medios-de-pago" target="_blank" href="/node/29">¿Cómo funciona?</a>');
				  	$('.page-cart-checkout #edit-panes-payment-payment-method--5 .form-item').append('<a class="medios-de-pago" target="_blank" href="/node/29">¿Cómo funciona?</a>');
          */
				}
			}
			
	});

		/*busqueda topicos*/

		$.fn.extend({

			coloresTopicos: function (){
				var numResult = $('.resultados-topicos .panel-col-last .view').find('div.views-row').length;
				for(var i =1;i <= numResult;i++ ){
					var tempTipo = $('.resultados-topicos .panel-col-last .view .views-row-'+i+' .views-field-type span').text();
					switch (tempTipo) {
						case 'Evento':
						$('.resultados-topicos .panel-col-last .view .views-row-'+i+'').addClass('color-magenta');
						break;

						case 'Publicación':
						$('.resultados-topicos .panel-col-last .view .views-row-'+i+'').addClass('color-verde');
						break;  

						case 'revista':

						$('.resultados-topicos .panel-col-last .view .views-row-'+i+'').addClass('color-verde');

						break; 

						case 'Curso':

						$('.resultados-topicos .panel-col-last .view .views-row-'+i+'').addClass('color-naranja');

						break;  

						case 'Libro':

						$('.resultados-topicos .panel-col-last .view .views-row-'+i+'').addClass('color-azul');

						break;

						default:

						$('.resultados-topicos .panel-col-last .view .views-row-'+i+'').addClass('color-magenta');
					}
				}
			},
			nombreStock: function(stock, btnRecibe, url){
				var num = $(stock).text();
				if ( num <= 0){
					$(btnRecibe).html('<a class="boton-mas-info" href="'+url+'">Más información</a>');                 
				}
				else{
					$(btnRecibe).find('input').val('Inscríbete');    
				}
			},
			revistaActual: function(){

				var revista = $('.page-revista-js .home-revista .views-field-title span a').text();
				var numdiv = $('.page-revista-js .view-home-articulos.view-display-id-block_3').find('div.views-row').length;
				var cont = 0;

				for(var i=0; i <= numdiv; i++){
					var revistart = $('.page-revista-js .view-home-articulos .views-row-'+i+' .views-field-field-revista .field-content').text();
					if (revistart == revista){
						$('.page-revista-js .view-home-articulos.view-display-id-block_3 .views-row-'+i+'').css('display','inline-block');
						cont ++;
					} 
					if(cont <= 6){
						$('.page-revista-js .view-home-articulos.view-display-id-block_3 .item-list').hide();
					}
					if(cont == 6){
						return false;
					}
				} 
			}
		});

//$.fn.coloresTopicos();
$.fn.revistaActual();
if($('.page-user.logged-in td.cart-block-summary-items').text() != 'Tienes (0) productos')
	jQuery('.page-user.logged-in.page-user-edit').find('.ds-form').after('<a class="ircheckout" href="/cart">Ir al carrito de compras</a>');
	//jQuery('.page-user.logged-in.page-user-edit').find('.group-one-column').prepend('<a class="ircheckout" href="/cart">Ir al carrito de compras</a>');



/*fin busqueda topicos*/

var num2 = $('.curso-detalle .total_cursos > span').text();

var nombre_curso = $.trim($('.curso-detalle .panel-2col .pane-node-title .pane-content').text());

if(num2 <= 0){
	$('.curso-detalle .pane-webform-client-block-364574').addClass('muestra-form');
	
	$('.curso-detalle .btn_next_curso').addClass('con-form');
	$('.curso-detalle .node-add-to-cart').addClass('no-show');
	$('.curso-detalle input#edit-submitted-nombre-del-curso').attr('value',nombre_curso);
	
}

else{
	$('.curso-detalle .btn_next_curso').hide();  
	$('.curso-detalle .form-actions input.node-add-to-cart').val('Inscríbete por '+$(".curso-detalle .pane-node-sell-price .sell-price .uc-price").text() +'');
}

		$('.detalle-revista .node-add-to-cart.form-submit').val('Inscríbete');
		/*placeholder para recibe nuestras novedades*/
		$('#webform-client-form-31 #edit-submitted-email').attr('placeholder', 'Digita tu correo');
		$('.page-user .block-system #user-login #edit-name').attr('placeholder', 'Correo electrónico');  
		$('.page-user .block-system #user-login #edit-pass').attr('placeholder', 'Contraseña');
		
		var myurl = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');    
		if (myurl == 'destination=cart/checkout'){
			jQuery("body").addClass("page-user-register");
			$('ul.primary li:first-child a').attr('href','/user/register?destination=cart/checkout');
			$('.page-user.not-logged-in .t-middle').prepend('<div><ul class="pasos"><li><span>1</span><p>CARRITO DE COMPRAS</p></li><li><span>2</span><p>IDENTIFICACIÓN</p></li><li><span>3</span><p>DATOS DE COMPRA Y ENVÍO</p></li><li><span>4</span><p>PAGO</p></li></ul></div>');
		}
		

		/*entrada libre eventos*/
		var entrada = jQuery('.curso-detalle .pane-node-field-entrada-libre .field-item.even').text();
		var cursoentrada = jQuery.trim(jQuery('.curso-detalle .panel-2col .pane-node-title .pane-content').text());
		if (entrada == 'entrada libre' || entrada == 'Entrada libre' || entrada == 'entrada-libre'){
			jQuery('.curso-detalle .panel-pane.pane-custom.pane-1').html('');
			//$('.curso-detalle .panel-pane.pane-custom.pane-1').html('<a class="entrada-libre" href="/content/entrada-libre?curso='+cursoentrada+'">Incribirse</a>');
			//oculta modulos de la derecha
			jQuery('.curso-detalle .panel-pane.pane-custom.pane-2 > .pane-content').hide();
			jQuery('.curso-detalle .panel-pane.pane-custom.pane-3').hide();
			jQuery('.curso-detalle #edit-submit').hide();
			jQuery('.curso-detalle .form-item.webform-component.webform-component-email').hide();
			jQuery('.curso-detalle .panel-pane.pane-entity-field-extra.pane-node-add-to-cart').hide();

			jQuery('.curso-detalle .pane-webform-client-block-364574.muestra-form').css('display', 'none !important');

			jQuery('.curso-detalle .field.field-name-uc-product-image').addClass('entrada-libre-titulo')
			jQuery('.curso-detalle .pane-node-field-field-fecha-inicio').css('border-top','1px solid #CCC');
		}
		var nombrecursoent = unescape(location.search.replace('?', '').split('=')[1]);
		$('.page-node-581726 .webform-client-form div #edit-submitted-nombre-del-curso').val(nombrecursoent);
	});



$( document ).ready(function() {
        /* FLAGTOREMOVE
	jQuery(".page-cart-checkout #edit-panes-delivery-delivery-city").val("");
        */
    jQuery('.order-review-table tr:nth-of-type(11) td').text('Resumen de compra')

        /* FLAGTOREMOVE
	jQuery('.page-user-register #edit-field-nombre-s-und-0-value').attr('tabindex','1');
	jQuery('.page-user-register #edit-field-nombre-s-und-0-value--2').attr('tabindex','1');
	jQuery('.page-user-register #edit-field-nombre-s-und-0-value--3').attr('tabindex','1');

	jQuery('.page-user-register #edit-mail').attr('tabindex','2');
	jQuery('.page-user-register #edit-mail--2').attr('tabindex','2');
	jQuery('.page-user-register #edit-mail--3').attr('tabindex','2');

	jQuery('.page-user-register #edit-pass-pass1').attr('tabindex','3');
	jQuery('.page-user-register #edit-pass-pass1--2').attr('tabindex','3');
	jQuery('.page-user-register #edit-pass-pass1--3').attr('tabindex','3');

	jQuery('.page-user-register #edit-pass-pass2').attr('tabindex','4');
	jQuery('.page-user-register #edit-pass-pass2--2').attr('tabindex','4');
	jQuery('.page-user-register #edit-pass-pass2--3').attr('tabindex','4');

	jQuery('.page-user-register #edit-field-user-ubicaci-n-und-0-country').attr('tabindex','5');
	jQuery('.page-user-register #edit-field-user-ubicaci-n-und-0-country--2').attr('tabindex','5');
	jQuery('.page-user-register #edit-field-user-ubicaci-n-und-0-country--3').attr('tabindex','5');

	jQuery('.page-user-register #edit-field-user-ubicaci-n-und-0-province').attr('tabindex','6');
	jQuery('.page-user-register #edit-field-user-ubicaci-n-und-0-province--2').attr('tabindex','6');
	jQuery('.page-user-register #edit-field-user-ubicaci-n-und-0-province--3').attr('tabindex','6');

	
	jQuery('.page-user-register #edit-field-user-ubicaci-n-und-0-city').attr('tabindex','7');
	jQuery('.page-user-register #edit-field-user-ubicaci-n-und-0-city--2').attr('tabindex','7');
	jQuery('.page-user-register #edit-field-user-ubicaci-n-und-0-city--3').attr('tabindex','7');

	jQuery('#edit-field-terminos-und-aceptar').attr('tabindex','8');
	jQuery('#edit-field-terminos-und-aceptar--2').attr('tabindex','8');
	jQuery('#edit-field-terminos-und-aceptar--3').attr('tabindex','8');	

	jQuery('#edit-mailchimp-lists-mailchimp-quiero-recibir-el-bolet-n-subscribe').attr('tabindex','9');
	jQuery('#edit-mailchimp-lists-mailchimp-quiero-recibir-el-bolet-n-subscribe--2').attr('tabindex','9');
	jQuery('#edit-mailchimp-lists-mailchimp-quiero-recibir-el-bolet-n-subscribe--3').attr('tabindex','9');
         FLAGTOREMOVE */

	jQuery('#edit-field-terminos-und-0').attr('checked', false);
	jQuery('#edit-field-terminos-und-0--2').attr('checked', false);
	jQuery('#edit-field-terminos-und-0--3').attr('checked', false);

	//jQuery('.tabs.primary li:nth-of-type(3) a').text('Editar mis datos');
	jQuery('.tabs.primary li:nth-of-type(4) a').text('Mis pedidos');

  if (jQuery('.page-user-password , .page-user-reset-').length == 0)
    jQuery('.page-user #edit-submit').val('Guardar cambios');

	jQuery('.page-user-changed-password .form-item-current-pass > label').text('Contraseña actual');

	if ( jQuery('.page-user.page-user- .messages.status ul li:first-child').text() == "Te has suscrito a nuestro boletín"){
		jQuery('.page-user.page-user- .messages.status').hide();
	}
/* FLAGTOREMOVE
	var nombre_apellidos = jQuery(".page-cart-checkout #edit-panes-delivery-delivery-first-name").val();
	var departamento = jQuery("#edit-panes-delivery-delivery-zone option:selected").val()
	var ciudad = jQuery(".page-cart-checkout #edit-panes-delivery-delivery-city").val();
	var direccion = jQuery(".page-cart-checkout #edit-panes-delivery-delivery-street1").val();
	var tipo = jQuery(".page-cart-checkout #edit-document-type option:selected").val();
	var numero = jQuery(".page-cart-checkout #edit-panes-delivery-delivery-last-name").val(); 
	var telefono = jQuery(".page-cart-checkout #edit-panes-delivery-delivery-phone").val(); 

	jQuery('.page-user-edit #edit-submit').val('Guardar cambios');

	if (nombre_apellidos === "" || departamento === "0" || ciudad === "" || direccion === "" || tipo === "0" || numero === "" || telefono == "" )
	{
		jQuery(".page-cart-checkout #edit-continue").attr("disabled", "disabled");
		jQuery(".page-cart-checkout #edit-continue").closest("div").addClass('desactivado');
	}
		

	jQuery(".page-cart-checkout input[type='text']").on('focusout', function(){
		console.log('salio input');
		var nombre_apellidos = jQuery(".page-cart-checkout #edit-panes-delivery-delivery-first-name").val();
		var departamento = jQuery("#edit-panes-delivery-delivery-zone option:selected").val()
		var ciudad = jQuery(".page-cart-checkout #edit-panes-delivery-delivery-city").val();
		var direccion = jQuery(".page-cart-checkout #edit-panes-delivery-delivery-street1").val();
		var tipo = jQuery(".page-cart-checkout #edit-document-type option:selected").val();
		var numero = jQuery(".page-cart-checkout #edit-panes-delivery-delivery-last-name").val(); 
		var telefono = jQuery(".page-cart-checkout #edit-panes-delivery-delivery-phone").val(); 
		console.log(nombre_apellidos);
		console.log(departamento);
		console.log(ciudad);
		console.log(direccion);
		console.log(tipo);
		console.log(numero);
		console.log(telefono);
		if (nombre_apellidos != "" && departamento != "0" && ciudad != "" && direccion != "" && tipo != "0" && numero != "" && telefono != "" ){
			jQuery(".page-cart-checkout #edit-continue").removeAttr("disabled", "disabled");
			jQuery(".page-cart-checkout #edit-continue").closest("div").removeClass('desactivado');
			console.log('se descativo por el input');
		}
		else{
			jQuery(".page-cart-checkout #edit-continue").attr("disabled", "disabled");
			jQuery(".page-cart-checkout #edit-continue").closest("div").addClass('desactivado');
		}
	});

	jQuery(".page-cart-checkout select").on('focusout', function(){
		console.log('salio select');
		var nombre_apellidos = jQuery(".page-cart-checkout #edit-panes-delivery-delivery-first-name").val();
		var departamento = jQuery("#edit-panes-delivery-delivery-zone option:selected").val()
		var ciudad = jQuery(".page-cart-checkout #edit-panes-delivery-delivery-city").val();
		var direccion = jQuery(".page-cart-checkout #edit-panes-delivery-delivery-street1").val();
		var tipo = jQuery(".page-cart-checkout #edit-document-type option:selected").val();
		var numero = jQuery(".page-cart-checkout #edit-panes-delivery-delivery-last-name").val(); 
		var telefono = jQuery(".page-cart-checkout #edit-panes-delivery-delivery-phone").val();
		if (nombre_apellidos != "" && departamento != "0" && ciudad != "" && direccion != "" && tipo != "0" && numero != "" && telefono != "" ){
			jQuery(".page-cart-checkout #edit-continue").removeAttr("disabled", "disabled");
			jQuery(".page-cart-checkout #edit-continue").closest("div").removeClass('desactivado');
			console.log('ery-zone option:selected").val()');
		var ciudad = jQuery(".page-cart-checkout #edit-panes-delivery-delivery-city").val();
		var direccion = jQuery(".page-cart-chese descativo por el input");
		}
		else{
			jQuery(".page-cart-checkout #edit-continue").attr("disabled", "disabled");
			jQuery(".page-cart-checkout #edit-continue").closest("div").addClass('desactivado');
		}
	});
        
          */

	if ($("body").hasClass("page-user-edit"))
	{
		jQuery('#edit-field-terminos-und-0').attr('checked', true);
		jQuery('#edit-field-terminos-und-0--2').attr('checked', true);
		jQuery('#edit-field-terminos-und-0--3').attr('checked', true);
	}
	if ($("body").hasClass("logged-in"))
		jQuery("#block-menu-menu-men-secundario").find("li").last().prev().find("a").attr("href", "/user/"+AC_GLOBAL_UID+"/edit")		


/* FLAGTOREMOVE  
	if(jQuery('.page-cart-checkout-review .order-review-table tr:nth-of-type(13) .data-col').text() == 'Consignación'){
		jQuery('.page-cart-checkout-review .order-review-table tr:nth-of-type(13)').after('<tr style="display: block"><td class="title-col">Número de cuenta:</td><td class="data-col">666 45724 549</td></tr>');
		jQuery('.page-cart-checkout-review .order-review-table tr:nth-of-type(13)').after('<tr style="display: block"><td class="title-col">Tipo:</td><td class="data-col">corriente</td></tr>');
		jQuery('.page-cart-checkout-review .order-review-table tr:nth-of-type(13)').after('<tr style="display: block"><td class="title-col">Nombre:</td><td class="data-col">Editorial Magisterio SA</td></tr>');
		jQuery('.page-cart-checkout-review .order-review-table tr:nth-of-type(13)').after('<tr style="display: block"><td class="title-col">Convenio:</td><td class="data-col">2020240</td></tr>');
		jQuery('.page-cart-checkout-review .order-review-table tr:nth-of-type(13)').after('<tr style="display: block"><td class="title-col">NIT:</td><td class="data-col">830053091-9</td></tr>');
		jQuery('.page-cart-checkout-review .order-review-table tbody tr td input[type="submit"]#edit-submit').replaceWith('<a class="btn-pago" href="/">Realizar pago</a>');
		jQuery('.page-cart-checkout-review .order-review-table tbody tr:nth-of-type(18)').hide();
		jQuery('.page-cart-checkout-review .order-review-table tbody tr:nth-of-type(16)').show();
    
	}
*/


	jQuery('.field.field-name-comment-body.field-type-text-long.field-label-above > .field-label').text('Comentario:');

	if(jQuery('.curso-detalle .cantidad_stock > span').text() == 0)
		jQuery('.curso-detalle .panel-pane.pane-custom.pane-3').hide();


	$('.libro-detalle .pane-node-field-link-bajo-demanda').append('<a href="'+$('.libro-detalle .pane-node-field-link-bajo-demanda .field-item').text()+'" class="comprar-demanda" target="_blank">Comprar</a>');

	if (jQuery('.libro-detalle .field-name-field-ebook').text() === 'ebook')
	$('.libro-detalle .pane-node-sell-price').prepend('<img src="/sites/all/imagenesfancybox/ebook.png"/>');
	else
	$('.libro-detalle .pane-node-sell-price').prepend('<img src="/sites/all/imagenesfancybox/libro.png"/>');
	$('.libro-detalle .pane-node-field-precio-bajo-demanda').prepend('<img src="/sites/all/imagenesfancybox/demanda.png"/>');

	$('#nacionales').addClass('active');
	$('#nacionales').on('click', function(){
		jQuery('#edit-field-nacional-value').val('0');
		jQuery('#edit-field-nacional-value').trigger('change');
		$('#nacionales').addClass('active');
		$('#internacionales').removeClass('active');

	});
	$('#internacionales').on('click', function(){
		jQuery('#edit-field-nacional-value').val('1');
		jQuery('#edit-field-nacional-value').trigger('change');
		$('#internacionales').addClass('active');
		$('#nacionales').removeClass('active');
	});

	jQuery('.page-node-82 .webform-confirmation > p').html(' Hemos recibido tu solicitud. Te enviaremos un respuesta lo más pronto posible. !Gracias!<br></br>');
	jQuery('.form-item-comment-body-und-0-value label').html('Haz tu comentario<span class="form-required" title="Este campo es obligatorio.">*</span>');

	setTimeout(function () {
		jQuery('.custom-filter ul li:nth-of-type(2)').trigger('click');	
	}, 10);
	var numdiv = $('.formacion-general.view-cursos').find('div.views-row').length;
	var numdivhome = $('.formacion-general .view-formacion').find('div.views-row').length;
	for (var i = 1; i <= numdiv; i++){
		var fecha = jQuery('.formacion-general.view-cursos .views-row-'+i+' .views-field-field-field-fecha-inicio .field-content').text();
		var precio = jQuery('.formacion-general.view-cursos .views-row-'+i+' .views-field-sell-price span span').text();
		var link = jQuery('.formacion-general.view-cursos .views-row-'+i+' .views-field-title span a').attr('href');  
		jQuery('.formacion-general.view-cursos .views-row-'+i+'').append('<a class="btn-ver-mas-cursos" href="'+link+'">Ver más</a>');
		if (precio == '$0')
			jQuery('.formacion-general.view-cursos .views-row-'+i+' .views-field-sell-price span span').text('Entrada libre');
		if (fecha == '')
			jQuery('.formacion-general.view-cursos .views-row-'+i+' .views-field-field-field-fecha-inicio .field-content').text('Abierto');
	}

	//home de formacion
	for (var i = 1; i <= numdivhome; i++){
		var link = jQuery('.formacion-general .view-formacion .views-row-'+i+' .views-field-title span a').attr('href');  
		var preciohome = jQuery('.formacion-general .view-formacion .views-row-'+i+' .views-field-sell-price span span').text();
		jQuery('.formacion-general .view-formacion .views-row-'+i+'').append('<a class="btn-ver-mas-cursos" href="'+link+'">Ver más</a>');
		if (preciohome == '$0')
			jQuery('.formacion-general .view-formacion .views-row-'+i+' .views-field-sell-price span span').text('Entrada libre');

	}

	function reinicia (){
                /* FLAGTOREMOVE*/
    var $form = $('form.uc-cart-checkout-form');
    var inputs =  ['delivery_first_name','delivery_city','delivery_postal_code','delivery_street1','delivery_last_name','delivery_phone','delivery_company'];
    var selects =  [/*'delivery_country',*/'delivery_zone','delivery_company'];
    for (var i=0;i<selects.length;i++) {
      var $s = $('select[name="panes[delivery][' + selects[i] + ']"]',$form);
      $s.val($('option',$s).slice(0,1).attr('value'));
    }
    for (var i=0;i<inputs.length;i++) {
      $('input[name="panes[delivery][' + inputs[i] + ']"]',$form).val('');
    }
		//$('#delivery-pane .ajax-processed, #delivery-pane .form-text').val(''); 
		//$('#delivery-pane .form-select option:first-child').attr('selected', 'selected'); 
                /**/
	}  



	$('.not-logged-in .form-item-field-terminos-und.form-type-radios > label').html('Acepto los <a href="/terminos" target="_blank">términos y condiciones de Magisterio.com.co</a><span class="form-required" title="Este campo es obligatorio.">*</span>');
	$('.not-logged-in .form-item-field-terminos-und--2.form-type-radios > label').html('Acepto los <a href="/terminos" target="_blank">términos y condiciones de Magisterio.com.co</a><span class="form-required" title="Este campo es obligatorio.">*</span>');
	$('.not-logged-in .form-item-field-terminos-und--3.form-type-radios > label').html('Acepto los <a href="/terminos" target="_blank">términos y condiciones de Magisterio.com.co</a><span class="form-required" title="Este campo es obligatorio.">*</span>');
	$('.not-logged-in .form-item-field-terminos-und--4.form-type-radios > label').html('Acepto los <a href="/terminos" target="_blank">términos y condiciones de Magisterio.com.co</a><span class="form-required" title="Este campo es obligatorio.">*</span>');
  //jQuery('.page-cart-checkout #edit-panes-billing-copy-address').trigger('click');
  $('.page-cart .t-middle').prepend('<div><ul class="pasos"><li><span>1</span><p>CARRITO DE COMPRAS</p></li><li><span>2</span><p>IDENTIFICACIÓN</p></li><li><span>3</span><p>DATOS DE COMPRA Y ENVÍO</p></li><li><span>4</span><p>PAGO</p></li></ul></div>');
  $('#delivery-pane').append('<div id="reiniciar-boton">Limpiar datos del formulario</div>')
  $('.page-user.not-logged-in .block-system .form-item.form-type-textfield.form-item-name input').removeAttr('placeholder');
  $('.page-user.not-logged-in .block-system .form-item.form-type-password.form-item-pass input').removeAttr('placeholder');
  $('.suscripciones #edit-attributes-4-9').attr('checked', true);
  /*metodos de pago*/
  
  /* FLAGTOREMOVE MARCA:AAAA
  	jQuery('.page-cart-checkout .form-radios div:nth-of-type(2)').append('<div id="pago-colombia-id"></div>');
	jQuery('#pago-colombia-id').html('');
	jQuery('#pago-colombia-id').html('<ul class="pago-colombia"><li>Tarjeta de crédito<img src="/sites/all/imagenesfancybox/visa.gif"/><img src="/sites/all/imagenesfancybox/master.gif"/><img src="/sites/all/imagenesfancybox/american.png"/><img src="/sites/all/imagenesfancybox/diners.gif"/></li><li>Pago con tarjeta débito de ahorros o corriente<img src="/sites/all/imagenesfancybox/pse.png"/></li><li>Pago en efectivo en bancos Bancolombia, Occidente Banco de Bogotá<img src="/sites/all/imagenesfancybox/codigo.png"/></li><li>Pago en efectivo en Puntos Vía Baloto<img src="/sites/all/imagenesfancybox/baloto.gif"/></li><li>Pago en efectivo en Efecty<img src="/sites/all/imagenesfancybox/efecty.png"/></li></ul>');
	jQuery('.page-cart-checkout .form-radios div:nth-of-type(2) label').html('Pago en Colombia');	
	jQuery('.page-cart-checkout .form-radios div:nth-of-type(1) label').html('Fuera de Colombia');
	jQuery('.page-cart-checkout .form-radios div:nth-of-type(1)').append('<div id="fuera-colombia-id"></div>');
	jQuery('#fuera-colombia-id').html('');
	jQuery('#fuera-colombia-id').html('<ul class="fuera-colombia pago-colombia"><li>Tarjeta de crédito <img src="/sites/all/imagenesfancybox/visa.gif"/><img src="/sites/all/imagenesfancybox/master.gif"/><img src="/sites/all/imagenesfancybox/american.png"/><img src="/sites/all/imagenesfancybox/diners.gif"/></li></ul>');
	$('.page-cart-checkout #edit-panes-payment-payment-method .form-item').append('<a class="medios-de-pago" target="_blank" href="/node/29">¿Cómo funciona?</a>');
	$('.page-cart-checkout #edit-panes-payment-payment-method--2 .form-item').append('<a class="medios-de-pago" target="_blank" href="/node/29">¿Cómo funciona?</a>');
	$('.page-cart-checkout #edit-panes-payment-payment-method--3 .form-item').append('<a class="medios-de-pago" target="_blank" href="/node/29">¿Cómo funciona?</a>');
	$('.page-cart-checkout #edit-panes-payment-payment-method--4 .form-item').append('<a class="medios-de-pago" target="_blank" href="/node/29">¿Cómo funciona?</a>');
	$('.page-cart-checkout #edit-panes-payment-payment-method--5 .form-item').append('<a class="medios-de-pago" target="_blank" href="/node/29">¿Cómo funciona?</a>');
  */

  $('.ajax-register-links-wrapper .ajax-register-links li.last a').text('Olvidé mi contraseña');
  $('.page-cart-checkout #customer-pane span').text('DATOS DE ENVÍO');

  var fechaActual = $('#block-block-29 h2').text().replace('</span>','').replace('<span>','');

  $('#block-block-29 h2').text(fechaActual);



  if( $('.curso-detalle .panel-col-first .pane-entity-field .field-name-field-tipo-de-curso .field-item').text() === 'Evento' ){

  	$('.page-node-69').addClass('color-magenta');

  }



  /*cambniar copy en home eventos de ver mapa*/  

  $('#block-views-proximos-eventos-page .views-field-field-g-lugar .location.map-link a').text('Ver mapa');

  $('#block-views-proximos-eventos-page .views-field-field-g-lugar .location.map-link a').attr('target','_blank');


  /*categorias menu*/

  //.categorias-menu #menu-categorias

  $('#edit-field-categor-a-tid option').each(function (index) {
  	$('ul#menu-categorias').append(' <li data-val="'+ $(this).val() +'">'+ $(this).text() +'</li>');
  	$('ul#menu-categorias li:first-child').text('Todas');
  });
  /**/

  $('ul#menu-categorias li').click(function (event){
  	var opcionCate = $(event.target).attr('data-val');
  	$('ul#menu-categorias li').removeClass('active');
  	$(event.target).addClass('active');
  	$('#edit-field-categor-a-tid option[value='+ opcionCate +']').attr('selected','selected' );
  	$('#edit-field-categor-a-tid').trigger('change');
  });

  /*create my own timeline*/

  var anioActual = (new Date);

  var anioStr = anioActual.getFullYear().toString();

  for (var i=0; i<=10;i++){
  	$('.page-revista-numeros-anteriores .myTimeline .timelineList').append('<li class="timelineDate">'+ anioStr-- +'</li>');
  }
  $('.page-revista-numeros-anteriores .timelineDate').click(function (event){
  	var timelineTar = $(event.target);
  	$('.timelineDate').removeClass('active');
  	$(timelineTar).addClass('active'); 
  	$('.page-revista-numeros-anteriores #edit-field-a-o-de-edici-n-value-value-date').val( timelineTar.text() );
  	$('.page-revista-numeros-anteriores #edit-field-a-o-de-edici-n-value-value-date').trigger('change');
  });

	/*
  $('.resultados-topicos .custom-filter ul li').click(function (event){
  	var opcionLista = $(event.target);
  	var opcionFiltro = "";
  	switch(opcionLista.text()){
  		case 'Libros':
  		opcionFiltro = 'product';
  		break;

  		case 'Revistas':
  		opcionFiltro = 'revista';
  		break;

  		case 'Cursos':
  		opcionFiltro = 'curso';
  		break;

  		case 'Artículos':
  		opcionFiltro = 'article';
  		break;

  		case 'Videos':
  		opcionFiltro = 'video';
  		break;

  		default:
  		opcionFiltro = 'product';
  	}

  	$('.resultados-topicos .custom-filter ul li').removeClass('active');
  	opcionLista.addClass('active');
  	$('.resultados-topicos #edit-type option[value='+ opcionFiltro +']').attr('selected','selected' );
  	$('.resultados-topicos #edit-type').trigger('change');

  });
  
  */
/* FLAGTOREMOVE 
 	jQuery('.page-cart-pagosonline-complete .t-content').append('<div class="datos-compra">');
 	if ( $('body').hasClass('page-cart-pagosonline-complete') && getUrlParameter('usuario_id') != ''){
	 	jQuery('.page-cart-pagosonline-complete .t-content .datos-compra').append('<h1>Detalles de transacción</h1>');
	 	jQuery('.page-cart-pagosonline-complete .t-content .datos-compra').append('<h5>Anota estos datos para posteriores consultas.</h5>');
	 	jQuery('.page-cart-pagosonline-complete .t-content .datos-compra').append(pintarResumen('Nombre de la empresa:', 'Editorial Magisterio'));
	 	jQuery('.page-cart-pagosonline-complete .t-content .datos-compra').append(pintarResumen('NIT:', '830053091-9'));
	 	jQuery('.page-cart-pagosonline-complete .t-content .datos-compra').append(pintarResumen('Fecha de procesamiento:', getUrlParameter('fecha_procesamiento') ) );
	 	jQuery('.page-cart-pagosonline-complete .t-content .datos-compra').append(pintarResumen('Estado de la transacción:',  getUrlParameter('mensaje').split('+')[1]) );
	 	jQuery('.page-cart-pagosonline-complete .t-content .datos-compra').append(pintarResumen('Referencia de la venta:',  getUrlParameter('ref_venta').split('+')[2]) );
	 	jQuery('.page-cart-pagosonline-complete .t-content .datos-compra').append(pintarResumen('Referencia de la transacción:',  getUrlParameter('ref_pol')));
	 	jQuery('.page-cart-pagosonline-complete .t-content .datos-compra').append(pintarResumen('Valor total:',  getUrlParameter('valor').split('.')[0]));
	 	jQuery('.page-cart-pagosonline-complete .t-content .datos-compra').append(pintarResumen('Moneda:',  getUrlParameter('moneda')));
 	}
 	else{
 		jQuery('.page-cart-pagosonline-complete .t-content .datos-compra').append('<h1>No hay datos</h1>');
 	}
 	jQuery('.page-cart-pagosonline-complete .t-content').append('</div>');
*/

	function pintarResumen(texto, valor){
		return('<div><strong>'+texto+'</strong><p>'+valor+'</p></div>');
	} 	
  $('#reiniciar-boton').on('click', function(){
  	reinicia();
  });

  	function getUrlParameter(sParam)
	{
	    var sPageURL = window.location.search.substring(1);
	    var sURLVariables = sPageURL.split('&');
	    for (var i = 0; i < sURLVariables.length; i++) 
	    {
	        var sParameterName = sURLVariables[i].split('=');
	        if (sParameterName[0] == sParam) 
	        {
	            return sParameterName[1];
	        }
	    }
	}   

//ACM
	(function() {
    $('#block-menu-menu-men-secundario li.last.leaf').css('position','relative').append('<div class="t-triangle-ledger" />');
	})();
	(function() {
    $('.page-user-orders- #block-system-main > .content').prepend('<a class="t-back-to-orders" href=".">Volver a mis pedidos.</a>');
	})();
	(function() {
    $('form.user_profile_form-change-password .form-item-pass-pass1 label').text('Nueva Contraseña');
    $('form.user_profile_form-change-password .form-item-pass-pass2 label').text('Confirmar Nueva Contraseña');
		//$('#line-items-div').append($('body.page-cart-checkout #edit-actions'));
	})();
	(function() {
    $('#user-profile-form.user_profile_form-preferences .field-type-datetime legend').replaceWith(function(){
      return $("<div />", {html: $(this).html(),class:'t-dev-legend'});
    });
	})();
	(function() {
    $('#customer-pane, #delivery-pane').wrapAll('<div id="t-customer-delivery-pane" class="form-wrapper" />');
	})();
  $("#_myFancyPopup").click();
});

})(jQuery);

